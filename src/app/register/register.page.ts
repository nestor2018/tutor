import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss']
})
export class RegisterPage implements OnInit {
  name: string;
  userName: string;
  lastName: string;
  password: string;
  document: string;
  typeUser: string;
  email: string;
  constructor(
    private router: Router,
    public alertController: AlertController
  ) {}

  ngOnInit() {}

  register() {
    fetch(environment.URL + 'user/registrar', {
      method: 'POST', // or 'PUT'
      body: new URLSearchParams({
        name: this.name,
        username: this.userName + this.lastName,
        password: this.password,
        documentId: this.document,
        typeuser: this.typeUser,
        email: this.email
      }), // data can be `string` or {object}!
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      }
    })
      .then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => {
        if (response.status == 200) {
          this.router.navigateByUrl('/login');
        } else {
          this.errorData();
        }
      });
  }

  async errorData() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Datos erroneos',
      message: 'Debes completar todos los campos.',
      buttons: ['Continuar']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }
}
