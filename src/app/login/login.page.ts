import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

import { StorageService } from '../services/storage.service';
import { environment } from '../../environments/environment.prod';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  constructor(
    private storage: StorageService,
    public alertController: AlertController,
    private router: Router
  ) {}
  user: string;
  password: string;
  isLoggedIn: any;
  ngOnInit() {
    this.storage.init();
    console.log(environment);
  }

  login() {
    fetch(environment.URL + 'user/login', {
      method: 'POST', // or 'PUT'
      body: new URLSearchParams({
        username: this.user,
        password: this.password
      }), // data can be `string` or {object}!
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      }
    })
      .then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => {
        if (response.body.token) {
          this.storage.set('token', response.body.token);
          this.storage.set('username', response.body.username);
          this.storage.set('typeUser', response.body.typeuser);
          this.getData();
        } else {
          this.errorData();
        }
      });
  }

  getData() {
    this.storage.get('token').then(data => {
      if (data) {
        this.router.navigateByUrl('/index');
      }
    });
  }

  async errorData() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Datos erroneos',
      message: 'Usuario o contraseña incorrectos.',
      buttons: ['Continuar']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }
}
