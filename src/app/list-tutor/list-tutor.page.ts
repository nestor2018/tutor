import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-tutor',
  templateUrl: './list-tutor.page.html',
  styleUrls: ['./list-tutor.page.scss']
})
export class ListTutorPage implements OnInit {
  list = [
    {
      nombre: 'pedro',
      facultad: 'Ingenieria',
      especialidad: 'Desarrollo',
      observacion: 'Experto en API',
      correo: 'pedro@gmail.com',
      telefono: 3151234567
    },
    {
      nombre: 'pedrito',
      facultad: 'Ingenieria',
      especialidad: 'Desarrollo',
      observacion: 'Experto en API',
      correo: 'pedro@gmail.com',
      telefono: 3151234567
    },
    {
      nombre: 'juan',
      facultad: 'Ingenieria',
      especialidad: 'Desarrollo',
      observacion: 'Experto en API',
      correo: 'pedro@gmail.com',
      telefono: 3151234567
    },
    {
      nombre: 'mauricio',
      facultad: 'Ingenieria',
      especialidad: 'Desarrollo',
      observacion: 'Experto en API',
      correo: 'pedro@gmail.com',
      telefono: 3151234567
    }
  ];
  items: any;
  search_name: string;
  constructor() {}

  ngOnInit() {
    this.initializeItems()
  }

	initializeItems() {
		this.items = this.list;
	}

	SearchgetItems(ev: any) {
		// Reset items back to all of the items
		this.initializeItems();

		// set val to the value of the searchbar
		const val = ev.target.value;

		// if the value is an empty string don't filter the items
		if (val && val.trim() != '') {
			this.items = this.items.filter((item) => {
				return item['nombre'].toLowerCase().indexOf(val.toLowerCase()) > -1;
			});
		}
	}
}
