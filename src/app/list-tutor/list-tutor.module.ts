import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListTutorPageRoutingModule } from './list-tutor-routing.module';

import { ListTutorPage } from './list-tutor.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListTutorPageRoutingModule
  ],
  declarations: [ListTutorPage]
})
export class ListTutorPageModule {}
