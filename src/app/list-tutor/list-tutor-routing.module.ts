import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListTutorPage } from './list-tutor.page';

const routes: Routes = [
  {
    path: '',
    component: ListTutorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListTutorPageRoutingModule {}
