import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RequestTutoringhPageRoutingModule } from './request-tutoringh-routing.module';

import { RequestTutoringhPage } from './request-tutoringh.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RequestTutoringhPageRoutingModule
  ],
  declarations: [RequestTutoringhPage]
})
export class RequestTutoringhPageModule {}
