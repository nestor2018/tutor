import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RequestTutoringhPage } from './request-tutoringh.page';

const routes: Routes = [
  {
    path: '',
    component: RequestTutoringhPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RequestTutoringhPageRoutingModule {}
