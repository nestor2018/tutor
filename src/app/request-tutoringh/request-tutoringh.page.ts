import { Component, OnInit } from '@angular/core';
import { StorageService } from '../services/storage.service';
import { environment } from '../../environments/environment.prod';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-request-tutoringh',
  templateUrl: './request-tutoringh.page.html',
  styleUrls: ['./request-tutoringh.page.scss']
})
export class RequestTutoringhPage implements OnInit {
  token: any;
  username: any;
  title: any;
  topic: any;
  description: any;
  constructor(
    private storage: StorageService,
    public alertController: AlertController
  ) {}

  ngOnInit() {
    this.getToken();
    this.getUsername();
  }

  requestTutoringh() {
    fetch(environment.URL + 'tutoria/solicitar', {
      method: 'POST', // or 'PUT'
      body: new URLSearchParams({
        username: this.username,
        title: this.title,
        topic: this.topic,
        description: this.description
      }), // data can be `string` or {object}!
      headers: {
        'Authorization': `Token ${this.token}`,
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      }
    })
      .then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => {
        if (response.status == 200) {
          this.information(
            'Solicitud correcta',
            'Tutoria solicidad correctamente'
          );
        } else {
          this.information('Datos erroneos', 'Debe completar todos los campos');
        }
      });
  }

  async information(title, message) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: title,
      message: message,
      buttons: ['Continuar']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  getToken() {
    this.storage.get('token').then(data => {
      this.token = data;
    });
  }
  getUsername() {
    this.storage.get('username').then(data => {
      this.username = data;
    });
  }
}
