import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { StorageService } from '../services/storage.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.page.html',
  styleUrls: ['./index.page.scss']
})
export class IndexPage implements OnInit {
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
  constructor(private router: Router, private storage: StorageService) {}

  ngOnInit() {}

  goPage(page: string) {
    this.router.navigateByUrl(page);
  }

  logout() {
    this.storage.set('token', null);
    this.goPage('/home');
  }
}
